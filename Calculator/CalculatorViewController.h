//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Osama Al-Najjar on 3/6/14.
//  Copyright (c) 2014 Osama Al-Najjar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController

// Here we define the current number and final result label object.
@property (weak, nonatomic) IBOutlet UILabel *consoleLabel;

// This is for the current result label object.
@property (weak, nonatomic) IBOutlet UILabel *currentResultLabel;

@property (weak, nonatomic) IBOutlet UILabel *plusSignLabel;

// Create two objects to detect the swipe on the result label 'consoleLabel'.
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *downSwipeGestureRecognizer;

// Count the number of numbers that were counted in the addition operator.
@property (nonatomic) int additionCounter;

// TODO Image
@property (nonatomic, strong) IBOutlet UIImageView *imageView;


@end
