//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Osama Al-Najjar on 3/6/14.
//  Copyright (c) 2014 Osama Al-Najjar. All rights reserved.
//

#import "CalculatorViewController.h"

@interface CalculatorViewController () {
    NSString *stack;    // The label that will display numbers and integers and the result;
    double result;      // The result of the calculation.
}

@end



@implementation CalculatorViewController
@synthesize consoleLabel, currentResultLabel, additionCounter, plusSignLabel, imageView;

- (void)viewDidLoad {
    [super viewDidLoad];
    stack = @"";
    [consoleLabel setText:@""];
    
    // Create the swipeRecognizer objects.
    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.downSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
    // Set the direction of the swipe.
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    self.downSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    
    // Add the gestures to the view.
    [self.view addGestureRecognizer:self.leftSwipeGestureRecognizer];
    [self.view addGestureRecognizer:self.rightSwipeGestureRecognizer];
    [self.view addGestureRecognizer:self.downSwipeGestureRecognizer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    // Trigger the shake gesture on.
    [self becomeFirstResponder];
    
    // Start the move in animation after 0.1f seconds.
    [self performSelector:@selector(moveIn:) withObject:currentResultLabel afterDelay:0.05f];
    
    additionCounter = 0;
}





- (IBAction)one:(id)sender
{
    [self addNumber:1];
}
- (IBAction)two:(id)sender
{
    [self addNumber:2];
}
- (IBAction)three:(id)sender
{
    [self addNumber:3];
}
- (IBAction)four:(id)sender
{
    [self addNumber:4];
}
- (IBAction)five:(id)sender
{
    [self addNumber:5];
}
- (IBAction)six:(id)sender
{
    [self addNumber:6];
}
- (IBAction)seven:(id)sender
{
    [self addNumber:7];
}
- (IBAction)eight:(id)sender
{
    [self addNumber:8];
}
- (IBAction)nine:(id)sender
{
    [self addNumber:9];
}
- (IBAction)zero:(id)sender
{
    [self addNumber:0];
}
- (IBAction)remove:(id)sender
{
    [self addNumber:-1];
}
- (IBAction)dot:(id)sender {
    [self addPoint];
}





/*
 * TODO DESCRIPTION
 */
- (IBAction)equal:(id)sender {
    
    // Add the last entered number to the result.
    [self add:self];
    
    // Convert the result to a string and save it in the stack var.
    stack = [NSString stringWithFormat:@"%.0f", result];
    
    [consoleLabel setText:stack];
    [currentResultLabel setText:[NSString stringWithFormat:@"%.1d", additionCounter]];
    stack = @"";
}



/*
 * Apply the addition operator.
 */
- (IBAction)add:(id)sender {
    result = result + [stack doubleValue];
    stack = @"";
    [currentResultLabel setText:[NSString stringWithFormat:@"%.0f", result]];
    additionCounter++;
    
    // Change the plusSignLabel color to green to give the user the feeling that its turned on.
    self.plusSignLabel.textColor = [UIColor greenColor];
}



/*
 * TODO DESCRIPTION
 */
- (IBAction)clear:(id)sender {
    stack = @"";
    result = 0;
    additionCounter = 0;
    [consoleLabel setText:stack];
    [currentResultLabel setText:stack];
}



/*
 * Add the number to the label 'stack'.
 * The add function requires a couple of important features such as checking if the stack
 * is NULL and ensuring that we cannot remove more than the length of the stack, these simple
 * features ensure the application does not crash and the application works smoother.
 */
- (void)addNumber:(int)number {
    if(stack == NULL)
    {
        stack = @"0";
    }
    
    if(number > -1)
    {
        stack = [NSString stringWithFormat:@"%1$@%2$d", stack, number];
    } else if ([stack length] > 0) {
        stack = [stack substringToIndex: number];
    }
    
    if([stack length] <= 0)
    {
        stack = @"";
    }
    [consoleLabel setText:stack];
    
    // Change the plusSignLabel color to gray to give the user the feeling that its turned off.
    self.plusSignLabel.textColor = [UIColor lightGrayColor];
}



/*
 *
 */
- (void)removeNumber:(id)sender {
    stack = [stack substringToIndex:stack.length-1];
    [consoleLabel setText:stack];
}



/*
 * Adds a decimal point to the stack label.
 */
- (void)addPoint {
    stack = [NSString stringWithFormat:@"%1$@.", stack];
    [consoleLabel setText:stack];
}


// ***** ***** ***** ***** *****    MOTION METHODS  ***** ***** ***** ***** ***** //

/*
 * Detect the end of the device shake, this will call the equal method to calculate the final result.
 */
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if(motion == UIEventSubtypeMotionShake)
    {
        [self equal:self];
    }
}



/*
 * Detect the finger swipe of the user.
 */
- (void)handleSwipes:(UISwipeGestureRecognizer *)sender {
    if(sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        [self clear:self];
    } else if (sender.direction == UISwipeGestureRecognizerDirectionDown) {
        [self add:self];
    } else if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        if([stack length] > 0)
            [self removeNumber:self];
    }
}



/*
 * Animate the object by a 'move in' animation.
 * Used at the viewDidLoad to animate the currentResultLabel.
 */
- (void)moveIn:(id)sender {
    // Assign the clicked button to a new object called button.
    //UIButton *button = (UIButton*)sender;
    UILabel *label = (UILabel*) sender;
    
    // Declare the button position and size;
    CGFloat xPos = label.frame.origin.x;
    CGFloat yPos = label.frame.origin.y;
    CGFloat width = label.frame.size.width;
    CGFloat height = label.frame.size.height;
    
    [UIView animateWithDuration:1.0 animations:^{
        label.transform = CGAffineTransformIdentity;
        label.frame = CGRectMake(xPos, yPos + 165, width, height);
    } completion:^(BOOL finished){}];
}



/*
 *
 */
- (IBAction)animateImage {
    imageView.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"myInfo.png"], nil];
    imageView.animationDuration = 5.00;
    imageView.animationRepeatCount = 1;
    [imageView startAnimating];
    [self.view addSubview:imageView];
}






@end
