//
//  main.m
//  Calculator
//
//  Created by Osama Al-Najjar on 3/6/14.
//  Copyright (c) 2014 Osama Al-Najjar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalculatorAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CalculatorAppDelegate class]));
    }
}
