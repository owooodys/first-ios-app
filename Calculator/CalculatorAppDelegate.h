//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Osama Al-Najjar on 3/6/14.
//  Copyright (c) 2014 Osama Al-Najjar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
